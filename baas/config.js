const isDocker = require("is-docker");

module.exports = {
  port: 3082,

  checkMac: false,

  version: "2.0",

  mysql: {
    host: "xxx",
    user: "xxx",
    password: "xxx",
    database: "xxx",
    charset: "utf8mb4"
  },

  redis: {
    host: "127.0.0.1",
    port: "6379"
  },

  qiniu: {
    accessKey: "xxx",
    secretKey: "xxx",
    bucket: "xxx"
  },

  sms: {
    appkey: "xxx",
    appsecret: "xxx",
    sms_free_sign_name: "xxx",
    sms_template_code: "xxx"
  },

  page: {
    pageSize: 20
  },

  webDir: "xxx",
  host: "xxx"
};
