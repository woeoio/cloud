# 微信小程序调试

##### 错误收集示例,实时监控小程序的错误。

```javascript
App({
    onLaunch: function(options) {
        // Do something initial when launch.
    },
    onShow: function(options) {
        // Do something when show.
    },
    onHide: function() {
        // Do something when hide.
    },
    onError: function(error) {
        wx.getSystemInfo({
            success: systemInfo => {
                wx.getNetworkType({
                    success: networkType => {
                        const err = error.split("\n");
                        wx.request({
                            url: 'https://baas.qingful.com/2.0/debug/add',
                            method: 'POST',
                            data: {
                                name: err[2],
                                type: err[0],
                                message: err[1],
                                info: error,
                                device: systemInfo.model,
                                system: systemInfo.system,
                                remark: `微信版本：${systemInfo.version},SDK版本：${systemInfo.SDKVersion},屏幕：宽：${systemInfo.screenWidth} 高：${systemInfo.screenHeight} 像素比：${systemInfo.pixelRatio},品牌：${systemInfo.brand},语言：${systemInfo.language}`
                            },
                            header: {
                                "x-qingful-appid": appId,
                                "x-qingful-appkey": appKey,
                                'content-type': 'application/json' // 默认值
                            },
                            success: function(res) {
                                console.log(res)
                            }
                        });
                    },
                    fail: res => {
                        console.error(
                            "getNetworkType Fail" + res.errMsg
                        );
                    }
                });
            },
            fail: res => {
                console.error(
                    "getSystemInfo Fail" + res.errMsg
                );
            }
        });
    },
    globalData: 'I am global data'
})
 ```

