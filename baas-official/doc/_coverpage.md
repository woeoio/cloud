<!-- ![logo](_media/logo.png) -->

# 青否云 <small></small>

> 一站式后端云服务解决方案。

- 为Web、小程序、 APP等应用，提供高效、简单、安全的后端云服务支持。支持一键生成源码、私有云等功能。
- 高效、简单、安全

[官方网站](https://cloud.qingful.com/)
[开源演示](https://github.com/qingful)
[查看文档](/zh-cn/summary)
[登录注册](https://dev.qingful.com/home/public/login)

